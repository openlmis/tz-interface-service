buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath "org.springframework.boot:spring-boot-gradle-plugin:2.2.2.RELEASE"
    }
}

plugins {
    id "org.flywaydb.flyway" version "6.0.8"
    id "org.sonarqube" version "2.6.2"
    id "com.moowork.node" version "1.2.0"
    id "io.freefair.lombok" version "4.1.6"
}

apply plugin: 'java'
apply plugin: 'idea'
apply plugin: 'org.springframework.boot'
apply plugin: 'io.spring.dependency-management'
apply plugin: 'checkstyle'
apply plugin: 'jacoco'
apply plugin: 'pmd'

group = serviceGroup
version = serviceVersion
archivesBaseName = rootProject.name
project.ext.buildTime = java.time.Instant.now().toString() // for versioning
sourceCompatibility = 1.8
targetCompatibility = 1.8

repositories {
    mavenCentral()
    jcenter()
    maven { url "https://oss.sonatype.org/content/repositories/snapshots" }
}

dependencies {
    compile "org.springframework.boot:spring-boot-starter-data-jpa"
    compile "org.springframework.boot:spring-boot-starter-data-rest"
    compile "org.springframework.boot:spring-boot-starter-web"
    compile "org.springframework.security.oauth.boot:spring-security-oauth2-autoconfigure:2.2.2.RELEASE"
    compile "org.postgresql:postgresql:42.0.0"
    compile "org.slf4j:slf4j-ext"
    compile 'commons-io:commons-io:2.5'
    compile 'org.apache.commons:commons-collections4:4.1'
    compile 'org.apache.commons:commons-csv:1.4'
    compile 'org.apache.commons:commons-lang3'
    compile 'org.flywaydb:flyway-core'
    compile 'org.javers:javers-spring-boot-starter-sql:2.8.1'
    compile 'org.openlmis:openlmis-service-util:3.1.0'
    compile 'org.webjars.npm:api-console:3.0.17'

    testCompile "junit:junit"
    testCompile "org.springframework.boot:spring-boot-starter-test"
    testCompile "org.springframework:spring-test"
    testCompile "com.github.tomakehurst:wiremock:1.58"

    testCompile "com.jayway.restassured:rest-assured:2.7.0"
    testCompile "guru.nidi.raml:raml-tester:0.8.15"
    testCompile "org.raml:raml-parser:0.8.37"

    testCompile "org.powermock:powermock-api-mockito2:2.0.4"
    testCompile "org.powermock:powermock-module-junit4:2.0.4"

    testCompile "nl.jqno.equalsverifier:equalsverifier:2.4"
    testCompile "be.joengenduvel.java.verifiers:to-string:1.0.2"
}

idea {
    project {
        vcs = 'Git'
        ipr.withXml { xmlFile ->
            // enable 'Annotation Processors', source: https://gist.github.com/stephanos/8645809
            xmlFile.asNode().component
                    .find { it.@name == 'CompilerConfiguration' }['annotationProcessing'][0]
                    .replaceNode {
                annotationProcessing {
                    profile(default: true, name: 'Default', useClasspath: 'true', enabled: true)
                }
            }
        }
    }
}

flyway {
    url = "$System.env.DATABASE_URL"
    user = "$System.env.POSTGRES_USER"
    password = "$System.env.POSTGRES_PASSWORD"
    schemas = ['interfaceservice']
    sqlMigrationPrefix = ''
    placeholderPrefix = '#['
    placeholderSuffix = ']'
}

sourceSets {
    integrationTest {
        java {
            compileClasspath += main.output + test.output
            runtimeClasspath += main.output + test.output
            srcDir file('src/integration-test/java')
        }
        resources.srcDir file('src/integration-test/resources')
    }
}

configurations {
    integrationTestCompile.extendsFrom testCompile
    integrationTestRuntime.extendsFrom testRuntime
}

task integrationTest(type: Test) {
    testClassesDirs = sourceSets.integrationTest.output.classesDirs
    classpath = sourceSets.integrationTest.runtimeClasspath
    testLogging {
        events "passed", "skipped", "failed"
        exceptionFormat = 'full'
    }
    mustRunAfter test
    environment 'BASE_URL', "http://localhost"
}

tasks.withType(Test) {
    reports.html.destination = file("${reporting.baseDir}/${name}")
    reports.junitXml.destination = file("${testResultsDir}/${name}")
}

// Usage: gradle generateMigration [-PmigrationName=name_of_migration]
//          Defaults to 'migration' as migration name
// Example: gradle generateMigration -PmigrationName=add_column_to_users
//          Will create a file in migration folder with name yyyyMMddHHmmssSSS_add_column_to_users.sql.
task generateMigration {
    description 'Creates an empty new file within the src/main/resources/db/migration directory into which developers can add new SQL migration code.'
    doLast {
        def fileName = project.hasProperty('migrationName') ? migrationName : 'migration'
        def timestamp = new Date().format('yyyyMMddHHmmssSSS', TimeZone.getTimeZone('GMT'))
        def fullFileName = "${timestamp}__${fileName}.sql"
        def migrationFile = new File(sourceSets.main.resources.srcDirs.first(), 'db/migration/' + fullFileName)
        migrationFile << "-- WHEN COMMITTING OR REVIEWING THIS FILE: Make sure that the timestamp in the file name (that serves as a version) is the latest timestamp, and that no new migration have been added in the meanwhile.\n"
        migrationFile << "-- Adding migrations out of order may cause this migration to never execute or behave in an unexpected way.\n"
        migrationFile << "-- Migrations should NOT BE EDITED. Add a new migration to apply changes."
        migrationFile.createNewFile()
    }
}


task checkApiIsRaml(type:Exec) {
    executable "raml-cop"
    args "src/main/resources/api-definition.yaml"
}

configure(checkApiIsRaml) {
    group = JavaBasePlugin.VERIFICATION_GROUP
    description = 'Verify that the api-specification is valid RAML'
}

jacocoTestReport {
    group = "reporting"
    description = "Generate Jacoco coverage reports after running tests."
    reports {
        xml.enabled false
        html.enabled true
        csv.enabled false
    }

    additionalSourceDirs.setFrom files(sourceSets.main.allJava.srcDirs)
}

checkstyle {
    toolVersion = "8.32"
}

//Usage: gradle sonarqube
sonarqube {
    properties {
        def branch = System.getenv("SONAR_BRANCH")
        if (branch && branch != 'master') {
            property "sonar.branch", branch
        }
        property "sonar.projectName", "OpenLMIS Interface Service"
        property "sonar.projectKey", "org.sonarqube:$rootProject.name"
        property "sonar.host.url", "http://sonar.openlmis.org"
        property "sonar.login", System.getenv("SONAR_LOGIN")
        property "sonar.password", System.getenv("SONAR_PASSWORD")
        property "sonar.projectVersion", version
        property "sonar.java.coveragePlugin", "jacoco"
        //Tells SonarQube where the unit tests execution reports are
        property "sonar.junit.reportsPath", "build/test-results/test"
        //Tells SonarQube where the unit tests code coverage report is
        property "sonar.jacoco.reportPath", "build/jacoco/test.exec"
        //Tells SonarQube where the integration tests code coverage report is
        property "sonar.jacoco.itReportPath", "build/jacoco/integrationTest.exec"
        properties["sonar.tests"] += sourceSets.integrationTest.java
    }
}

project.tasks["sonarqube"].dependsOn integrationTest

pmd {
    toolVersion = '5.8.1'
    consoleOutput = true
    ignoreFailures = false
    ruleSetFiles = files("config/pmd/ruleset.xml")
    reportsDir = file("build/reports/pmd")
    incrementalAnalysis = false
}

tasks.withType(Pmd){
    reports {
        xml.enabled true
        html.enabled true
    }
}


test {
    testLogging {
        events 'started', 'passed'
        exceptionFormat = 'full'
    }
}

apply from: "documentation.gradle"
integrationTest {
    dependsOn ramlToHtml
}

processResources {
    // we want the generated HTML spec file included in the output jar
    finalizedBy ramlToHtml

    // update version information in build
    filesMatching('**/version.properties') {
        expand(project.properties)
    }
}

apply from: "registration.gradle"

assemble {
    dependsOn ramlToHtml
    dependsOn copyRamlHtmlToBuild
    dependsOn copyConsulRegistrationToBuild
    dependsOn jacocoTestReport
}

check {
    dependsOn ramlToHtml
    dependsOn copyRamlHtmlToBuild
    dependsOn integrationTest
    dependsOn copyConsulRegistrationToBuild
}

build {
    dependsOn jacocoTestReport
    dependsOn check
}
