/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.interfaceservice.i18n;

import java.util.Arrays;

public abstract class MessageKeys {
  private static final String DELIMITER = ".";

  public static final String SERVICE_PREFIX = "interfaceservice";
  static final String SERVICE_ERROR_PREFIX = join(SERVICE_PREFIX, "error");
  private static final String ERROR = "error";

  private static final String WIDGET = "widget";
  private static final String JAVERS = "javers";

  private static final String ID = "id";
  private static final String CODE = "code";

  private static final String MISMATCH = "mismatch";
  public static final String NOT_FOUND = "notFound";
  private static final String DUPLICATED = "duplicated";

  public static final String ERROR_PREFIX = join(SERVICE_PREFIX, ERROR);

  public static final String ERROR_SERVICE_REQUIRED = ERROR_PREFIX + ".service.required";
  public static final String ERROR_SERVICE_OCCURED = ERROR_PREFIX + ".service.errorOccured";

  public static final String ERROR_WIDGET_NOT_FOUND = join(ERROR_PREFIX, WIDGET, NOT_FOUND);
  public static final String ERROR_WIDGET_ID_MISMATCH = join(ERROR_PREFIX, WIDGET, ID, MISMATCH);
  public static final String ERROR_WIDGET_CODE_DUPLICATED =
      join(ERROR_PREFIX, WIDGET, CODE, DUPLICATED);

  public static final String ERROR_JAVERS_EXISTING_ENTRY =
      join(ERROR_PREFIX, JAVERS, "entryAlreadyExists");

  public static final String ERROR_MISSING_MANDATORY_FIELD = SERVICE_PREFIX
          + ".missingMandatoryField";

  public static String join(String... params) {
    return String.join(DELIMITER, Arrays.asList(params));
  }

  protected MessageKeys() {
    throw new UnsupportedOperationException();
  }

}
