/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.interfaceservice.domain;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "hfr_facilities")
@NoArgsConstructor
public class HfrFacility extends BaseEntity {

  private static final String TEXT = "text";

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String commFacName;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String council;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private Date createAt;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String district;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String facIdNumber;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String facilityTypeGroup;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String latitude;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String longitude;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String name;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String osChangeClosedToOperational;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String osChangeOpenedToClose;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String operatingStatus;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String ownership;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String ownershipGroup;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String postOrUpdate;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String region;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String registrationStatus;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private Date updatedAt;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String village;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String ward;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String zone;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String districtCode;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String councilCode;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String facilityTypeGroupCode;

  @Column(nullable = false, columnDefinition = TEXT)
  @Getter
  private String ownershipCode;

  /**
   * Static factory method for constructing HFR facility.
   */
  public HfrFacility(String commFacName, String council, Date createAt,
                     String district, String facIdNumber, String facilityTypeGroup,
                     String latitude, String longitude, String name,
                     String osChangeClosedToOperational, String osChangeOpenedToClose,
                     String operatingStatus, String ownership, String ownershipGroup,
                     String postOrUpdate, String region, String registrationStatus,
                     Date updatedAt, String village, String ward, String zone, String districtCode,
                     String councilCode, String facilityTypeGroupCode, String ownershipCode,
                     String msdCode, String activatedByMsd, Date activatedDate) {
    this.commFacName = commFacName;
    this.council = council;
    this.createAt = createAt;
    this.district = district;
    this.facIdNumber = facIdNumber;
    this.facilityTypeGroup = facilityTypeGroup;
    this.latitude = latitude;
    this.longitude = longitude;
    this.name = name;
    this.osChangeClosedToOperational = osChangeClosedToOperational;
    this.osChangeOpenedToClose = osChangeOpenedToClose;
    this.operatingStatus = operatingStatus;
    this.ownership = ownership;
    this.ownershipGroup = ownershipGroup;
    this.postOrUpdate = postOrUpdate;
    this.region = region;
    this.registrationStatus = registrationStatus;
    this.updatedAt = updatedAt;
    this.village = village;
    this.ward = ward;
    this.zone = zone;
    this.districtCode = districtCode;
    this.councilCode = councilCode;
    this.facilityTypeGroupCode = facilityTypeGroupCode;
    this.ownershipCode = ownershipCode;
  }

  /**
   * Static factory method for constructing a new HFR facility.
   *
   * @param hfrFacility HFR facility
   */
  public void updateFrom(HfrFacility hfrFacility) {

    this.commFacName = hfrFacility.getCommFacName();
    this.council = hfrFacility.getCouncil();
    this.createAt = hfrFacility.getCreateAt();
    this.district = hfrFacility.getDistrict();
    this.facIdNumber = hfrFacility.getFacIdNumber();
    this.facilityTypeGroup = hfrFacility.getFacilityTypeGroup();
    this.latitude = hfrFacility.getLatitude();
    this.longitude = hfrFacility.getLongitude();
    this.name = hfrFacility.getName();
    this.osChangeClosedToOperational = hfrFacility.getOsChangeClosedToOperational();
    this.osChangeOpenedToClose = hfrFacility.getOsChangeOpenedToClose();
    this.operatingStatus = hfrFacility.getOperatingStatus();
    this.ownership = hfrFacility.getOwnership();
    this.ownershipGroup = hfrFacility.getOwnershipGroup();
    this.postOrUpdate = hfrFacility.getPostOrUpdate();
    this.region = hfrFacility.getRegion();
    this.registrationStatus = hfrFacility.getRegistrationStatus();
    this.updatedAt = hfrFacility.getUpdatedAt();
    this.village = hfrFacility.getVillage();
    this.ward = hfrFacility.getWard();
    this.zone = hfrFacility.getZone();
    this.districtCode = hfrFacility.getDistrictCode();
    this.councilCode = hfrFacility.getCouncilCode();
    this.facilityTypeGroupCode = hfrFacility.getFacilityTypeGroupCode();
    this.ownershipCode = hfrFacility.getOwnershipCode();
  }

  /**
   * Static factory method for constructing a new HFR facility using an importer (DTO).
   *
   * @param importer the HFR Facility importer (DTO)
   */
  public static HfrFacility newHfrFacility(
          HfrFacility.Importer importer) {
    HfrFacility newHfrFacility = new HfrFacility();

    newHfrFacility.commFacName = importer.getCommFacName();
    newHfrFacility.council = importer.getCouncil();
    newHfrFacility.createAt = importer.getCreateAt();
    newHfrFacility.district = importer.getDistrict();
    newHfrFacility.facIdNumber = importer.getFacIdNumber();
    newHfrFacility.facilityTypeGroup = importer.getFacilityTypeGroup();
    newHfrFacility.latitude = importer.getLatitude();
    newHfrFacility.longitude = importer.getLongitude();
    newHfrFacility.name = importer.getName();
    newHfrFacility.osChangeClosedToOperational = importer.getOsChangeClosedToOperational();
    newHfrFacility.osChangeOpenedToClose = importer.getOsChangeOpenedToClose();
    newHfrFacility.operatingStatus = importer.getOperatingStatus();
    newHfrFacility.ownership = importer.getOwnership();
    newHfrFacility.ownershipGroup = importer.getOwnershipGroup();
    newHfrFacility.postOrUpdate = importer.getPostOrUpdate();
    newHfrFacility.region = importer.getRegion();
    newHfrFacility.registrationStatus = importer.getRegistrationStatus();
    newHfrFacility.updatedAt = importer.getUpdatedAt();
    newHfrFacility.village = importer.getVillage();
    newHfrFacility.ward = importer.getWard();
    newHfrFacility.zone = importer.getZone();
    newHfrFacility.districtCode = importer.getDistrictCode();
    newHfrFacility.councilCode = importer.getCouncilCode();
    newHfrFacility.facilityTypeGroupCode = importer.getFacilityTypeGroupCode();
    newHfrFacility.ownershipCode = importer.getOwnershipCode();

    return newHfrFacility;
  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(HfrFacility.Exporter exporter) {
    exporter.setId(id);
    exporter.setName(name);
    exporter.setCouncil(council);
    exporter.setCreateAt(createAt);
    exporter.setDistrict(district);
    exporter.setFacIdNumber(facIdNumber);
    exporter.setFacilityTypeGroup(facilityTypeGroup);
    exporter.setLatitude(latitude);
    exporter.setLongitude(longitude);
    exporter.setName(name);
    exporter.setOsChangeClosedToOperational(osChangeClosedToOperational);
    exporter.setOsChangeOpenedToClose(osChangeOpenedToClose);
    exporter.setOperatingStatus(operatingStatus);
    exporter.setOwnership(ownership);
    exporter.setOwnershipGroup(ownershipGroup);
    exporter.setPostOrUpdate(postOrUpdate);
    exporter.setRegion(region);
    exporter.setRegistrationStatus(registrationStatus);
    exporter.setUpdatedAt(updatedAt);
    exporter.setVillage(village);
    exporter.setWard(ward);
    exporter.setZone(zone);
    exporter.setDistrictCode(districtCode);
    exporter.setCouncilCode(councilCode);
    exporter.setFacilityTypeGroup(facilityTypeGroup);
    exporter.setOwnershipCode(ownershipCode);
    exporter.setFacilityTypeGroupCode(facilityTypeGroupCode);
    exporter.setCommFacName(commFacName);

  }

  public interface Exporter {
    void setId(UUID id);

    public void setCommFacName(String commFacName);

    public void setCouncil(String council);

    public void setCreateAt(Date createAt);

    public void setDistrict(String district);

    public void setFacIdNumber(String facIdNumber);

    public void setFacilityTypeGroup(String facilityTypeGroup);

    public void setLatitude(String latitude);

    public void setLongitude(String longitude);

    public void setName(String name);

    public void setOsChangeClosedToOperational(String osChangeClosedToOperational);

    public void setOsChangeOpenedToClose(String osChangeOpenedToClose);

    public void setOperatingStatus(String operatingStatus);

    public void setOwnership(String ownership);

    public void setOwnershipGroup(String ownershipGroup);

    public void setPostOrUpdate(String postOrUpdate);

    public void setRegion(String region);

    public void setRegistrationStatus(String registrationStatus);

    public void setUpdatedAt(Date updatedAt);

    public void setVillage(String village);

    public void setWard(String ward);

    public void setZone(String zone);

    public void setDistrictCode(String districtCode);

    public void setCouncilCode(String councilCode);

    public void setFacilityTypeGroupCode(String facilityTypeGroupCode);

    public void setOwnershipCode(String ownershipCode);

  }

  public interface Importer {

    UUID getId();

    String getCommFacName();

    String getCouncil();

    Date getCreateAt();

    String getDistrict();

    String getFacIdNumber();

    String getFacilityTypeGroup();

    String getLatitude();

    String getLongitude();

    String getName();

    String getOsChangeClosedToOperational();

    String getOsChangeOpenedToClose();

    String getOperatingStatus();

    String getOwnership();

    String getOwnershipGroup();

    String getPostOrUpdate();

    String getRegion();

    String getRegistrationStatus();

    Date getUpdatedAt();

    String getVillage();

    String getWard();

    String getZone();

    String getDistrictCode();

    String getCouncilCode();

    String getFacilityTypeGroupCode();

    String getOwnershipCode();
  }

}
