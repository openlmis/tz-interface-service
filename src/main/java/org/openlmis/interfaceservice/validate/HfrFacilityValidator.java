/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.interfaceservice.validate;

import org.openlmis.interfaceservice.dto.HfrFacilityDto;
import org.openlmis.interfaceservice.i18n.MessageKeys;
import org.openlmis.interfaceservice.util.Message;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * A validator for {@link org.openlmis.interfaceservice.dto.HfrFacilityDto} object.
 */
@Component
public class HfrFacilityValidator implements BaseValidator {

  static final String ownershipCode = "ownershipCode";
  static final String facilityTypeGroupCode = "facilityTypeGroupCode";
  static final String councilCode = "councilCode";
  static final String districtCode = "districtCode";
  static final String zone = "zone";
  static final String ward = "ward";
  static final String village = "village";
  static final String updatedAt = "updatedAt";
  static final String registrationStatus = "registrationStatus";
  static final String region = "region";
  static final String postOrUpdate = "postOrUpdate";
  static final String ownershipGroup = "ownershipGroup";
  static final String ownership = "ownership";
  static final String operatingStatus = "operatingStatus";
  static final String osChangeOpenedToClose = "osChangeOpenedToClose";
  static final String osChangeClosedToOperational = "osChangeClosedToOperational";
  static final String name = "name";
  static final String longitude = "longitude";
  static final String latitude = "latitude";
  static final String facilityTypeGroup = "facilityTypeGroup";
  static final String facIdNumber = "facIdNumber";
  static final String district = "district";
  static final String createAt = "createAt";
  static final String council = "council";
  static final String commFacName = "commFacName";

  /**
   * Checks if the given class definition is supported.
   *
   * @param clazz the {@link Class} that this {@link Validator} is being asked if it can {@link
   *              #validate(Object, Errors) validate}
   * @return true if {@code clazz} is equal to
   * {@link HfrFacilityDto} class definition.Otherwise false.
   */
  @Override
  public boolean supports(Class<?> clazz) {
    return HfrFacilityDto.class.equals(clazz);
  }

  /**
   * Validates the {@code target} object, which must be an instance of
   * {@link HfrFacilityDto} class.
   * <p>Firstly, the method checks if the target object has a value in {@code code} and {@code name}
   * properties. For those two properties the value cannot be {@code null}, empty or
   * contains only whitespaces.</p>
   * <p>If there are no errors, the method checks if the {@code code} property in the target
   * object is unique.</p>
   *
   * @param target the object that is to be validated (never {@code null})
   * @param errors contextual state about the validation process (never {@code null})
   * @see ValidationUtils
   */
  @Override
  public void validate(Object target, Errors errors) {
    verifyArguments(target, errors, MessageKeys.ERROR_MISSING_MANDATORY_FIELD);
    verifyProperties(errors);
  }

  private void verifyProperties(Errors errors) {

    rejectIfNull(errors, ownershipCode,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, ownershipCode));
    rejectIfNull(errors, operatingStatus,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, operatingStatus));
    rejectIfNull(errors, ownership,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, ownership));
    rejectIfNull(errors, osChangeClosedToOperational,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, osChangeClosedToOperational));
    rejectIfNull(errors, osChangeOpenedToClose,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, osChangeOpenedToClose));
    rejectIfNull(errors, facilityTypeGroupCode,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, facilityTypeGroupCode));
    rejectIfNull(errors, facIdNumber,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, facIdNumber));
    rejectIfNull(errors, facilityTypeGroup,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, facilityTypeGroup));
    rejectIfNull(errors, commFacName,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, commFacName));
    rejectIfNull(errors, latitude,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, latitude));
    rejectIfNull(errors, longitude,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, longitude));
    rejectIfNull(errors, council,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, council));
    rejectIfNull(errors, councilCode,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, councilCode));
    rejectIfNull(errors, district,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, district));
    rejectIfNull(errors, districtCode,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, districtCode));
    rejectIfNull(errors, zone,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, zone));
    rejectIfNull(errors, ward,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, ward));
    rejectIfNull(errors, village,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, village));
    rejectIfNull(errors, registrationStatus,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, registrationStatus));
    rejectIfNull(errors, updatedAt,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, updatedAt));
    rejectIfNull(errors, postOrUpdate,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, postOrUpdate));
    rejectIfNull(errors, region,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, region));
    rejectIfNull(errors, name,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, name));
    rejectIfNull(errors, ownershipGroup,
            new Message(MessageKeys.ERROR_MISSING_MANDATORY_FIELD, ownershipGroup));
  }
}