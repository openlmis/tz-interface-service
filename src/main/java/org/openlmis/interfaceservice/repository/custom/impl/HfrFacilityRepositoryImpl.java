/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.interfaceservice.repository.custom.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.openlmis.interfaceservice.domain.HfrFacility;
import org.openlmis.interfaceservice.repository.custom.HfrFacilityRepositoryCustom;

public class HfrFacilityRepositoryImpl implements HfrFacilityRepositoryCustom {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public List<HfrFacility> getFacilityByHfrCode(String hfrCode) {
    CriteriaBuilder builder = entityManager.getCriteriaBuilder();
    CriteriaQuery<HfrFacility> query = builder.createQuery(HfrFacility.class);
    Root<HfrFacility> root = query.from(HfrFacility.class);
    Predicate predicate = builder.conjunction();
    predicate = addEqualsFilter(predicate, builder, root, "facIdNumber", hfrCode);
    query.where(predicate);
    List<HfrFacility> results = entityManager.createQuery(query).getResultList();
    return results;
  }

  private Predicate addEqualsFilter(Predicate predicate, CriteriaBuilder builder, Root root,
                                    String filterKey, Object filterValue) {
    if (filterValue != null) {
      return builder.and(
              predicate,
              builder.equal(
                      root.get(filterKey), filterValue));
    } else {
      return predicate;
    }
  }
}
