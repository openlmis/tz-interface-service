/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.interfaceservice.web;

import com.google.common.collect.Sets;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.NoArgsConstructor;
import org.openlmis.interfaceservice.domain.HfrFacility;
import org.openlmis.interfaceservice.dto.HfrFacilityDto;
import org.openlmis.interfaceservice.exception.ValidationMessageException;
import org.openlmis.interfaceservice.service.HfrIntegrationService;
import org.openlmis.interfaceservice.service.PermissionService;
import org.openlmis.interfaceservice.validate.HfrFacilityValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@NoArgsConstructor
public class HfrIntegrationController extends BaseController {

  private static final Logger LOGGER = LoggerFactory.getLogger(HfrIntegrationController.class);

  @Autowired
  private HfrIntegrationService service;

  @Autowired
  private PermissionService permissionService;

  @Autowired
  private HfrFacilityValidator validator;

  /**
   * Save HFR Facility.
   *
   * @return HFR facility.
   */
  @PostMapping("/hfrFacilities")
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public HfrFacilityDto saveNewFacility(@RequestBody HfrFacilityDto hfrFacilityDto,
                                        BindingResult bindingResult) {

    validator.validate(hfrFacilityDto, bindingResult);
    if (bindingResult.getErrorCount() > 0) {
      throw new ValidationMessageException(bindingResult.getFieldError().getDefaultMessage());
    }

    permissionService.canAddHfrFacility();

    HfrFacility toSaveHfrFacility = HfrFacility.newHfrFacility(hfrFacilityDto);
    toSaveHfrFacility = service.saveHfrFacility(toSaveHfrFacility);

    LOGGER.error("Saved rejection reasons with id: " + toSaveHfrFacility.getId());

    return exportToDto(toSaveHfrFacility);
  }

  /**
   * Update HFR facility in the system.
   *
   * @return HFR facility in the system.
   */
  @PutMapping("/hfrFacilities")
  @ResponseStatus(HttpStatus.ACCEPTED)
  @ResponseBody
  public HfrFacilityDto updateHfrFacility(@RequestBody HfrFacilityDto hfrFacilityDto,
                                          BindingResult bindingResult) {

    validator.validate(hfrFacilityDto, bindingResult);
    if (bindingResult.getErrorCount() > 0) {
      throw new ValidationMessageException(bindingResult.getFieldError().getDefaultMessage());
    }

    permissionService.canAddHfrFacility();

    List<HfrFacility> hfrFacilityByCode =
            service.getHfrFacilityByCode(hfrFacilityDto.getFacIdNumber());

    HfrFacility toSaveHfrFacility;
    HfrFacility template = HfrFacility.newHfrFacility(hfrFacilityDto);

    if (hfrFacilityByCode.size() == 0) {

      toSaveHfrFacility = HfrFacility.newHfrFacility(hfrFacilityDto);
      toSaveHfrFacility = service.saveHfrFacility(toSaveHfrFacility);

      toSaveHfrFacility = service.saveHfrFacility(toSaveHfrFacility);
    } else {
      toSaveHfrFacility = hfrFacilityByCode.get(0);
      toSaveHfrFacility.updateFrom(template);
    }

    toSaveHfrFacility = service.saveHfrFacility(toSaveHfrFacility);
    return exportToDto(toSaveHfrFacility);
  }

  /**
   * Get all HFR facilities in the system.
   *
   * @return all HFR facilities in the system.
   */
  @GetMapping("/hfrFacilities")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<HfrFacilityDto> getAllHfrFacility(Pageable pageable) {

    permissionService.canAddHfrFacility();

    Set<HfrFacility> hfrFacilities = Sets
            .newHashSet(service.getAllFacilities());
    Set<HfrFacilityDto> hfrFacilitiesDto =
            hfrFacilities.stream().map(this::exportToDto).collect(Collectors.toSet());

    return toPage(hfrFacilitiesDto, pageable);
  }

  /**
   * Export HFR facility to HFR facility DTO./widget.json
   */
  public HfrFacilityDto exportToDto(HfrFacility hfrFacility) {
    HfrFacilityDto hfrFacilityDto = new HfrFacilityDto();
    hfrFacility.export(hfrFacilityDto);
    return hfrFacilityDto;
  }

}
