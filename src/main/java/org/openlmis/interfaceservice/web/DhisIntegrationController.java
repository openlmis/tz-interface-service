/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.interfaceservice.web;

import java.util.List;
import org.openlmis.interfaceservice.domain.DhisDataset;
import org.openlmis.interfaceservice.service.MeasureReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class DhisIntegrationController extends BaseController {

  @Autowired
  private MeasureReportService service;

  @Value("dhis2.quantityReceived.id")
  private String receivedQuantityDatasetId;

  @Value("dhis2.quantityReceived.searchKey")
  private String receivedQuantitySearchKey;

  @Value("dhis2.stockAvailable.id")
  private String stockAvailableDatasetId;

  @Value("dhis2.stockAvailable.searchKey")
  private String stockAvailableSearchKey;

  @Value("dhis2.orderedQuantity.id")
  private String orderedQuantityId;

  @Value("dhis2.orderedQuantity.searchKey")
  private String orderedQuantitySearchKey;

  @Value("dhis2.monthlyStockOnHand.id")
  private String monthlyStockOnHandId;

  @Value("dhis2.monthlyStockOnHand.searchKey")
  private String monthlyStockOnHandSearchKey;

  @Value("dhis2.expectedIncidences.id")
  private String expectedIncidencesId;

  @Value("dhis2.expectedIncidences.searchKey")
  private String expectedIncidencesSearchKey;

  @GetMapping("dhis/receivedQuantity")
  @ResponseStatus(HttpStatus.ACCEPTED)
  @ResponseBody
  private List<DhisDataset> getReceivedQuantity(@RequestParam String startDate,
                                                @RequestParam String endDate) {
    return service.getDhisDataset(startDate, endDate,
            receivedQuantityDatasetId, stockAvailableSearchKey);
  }

  @GetMapping("dhis/stockAvailable")
  @ResponseStatus(HttpStatus.ACCEPTED)
  @ResponseBody
  private List<DhisDataset> stockAvailable(@RequestParam String startDate,
                                           @RequestParam String endDate) {
    return service.getDhisDataset(startDate, endDate,
            stockAvailableDatasetId, receivedQuantitySearchKey);
  }

  @GetMapping("/dhis/orderedQuantity")
  @ResponseStatus(HttpStatus.ACCEPTED)
  @ResponseBody
  private List<DhisDataset> getOrderedQuantity(@RequestParam String startDate,
                                               @RequestParam String endDate) {

    return service.getDhisDataset(startDate, endDate,
            orderedQuantityId, orderedQuantitySearchKey);
  }

  @GetMapping("/dhis/monthlyStockOnHand")
  @ResponseStatus(HttpStatus.ACCEPTED)
  @ResponseBody
  private List<DhisDataset> getMonthlyStockOnHand(@RequestParam String startDate,
                                                  @RequestParam String endDate) {

    return service.getDhisDataset(startDate, endDate,
            monthlyStockOnHandId, monthlyStockOnHandSearchKey);
  }

  @GetMapping("/dhis/expectedIncidences")
  @ResponseStatus(HttpStatus.ACCEPTED)
  @ResponseBody
  private List<DhisDataset> getExpectedIncidences(@RequestParam String startDate,
                                                  @RequestParam String endDate) {
    
    return service.getDhisDataset(startDate, endDate,
            expectedIncidencesId, expectedIncidencesSearchKey);
  }

}


