/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.interfaceservice.service;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Location;
import org.hl7.fhir.r4.model.MeasureReport;
import org.hl7.fhir.r4.model.MeasureReport.MeasureReportGroupComponent;
import org.openlmis.interfaceservice.domain.DhisDataset;
import org.openlmis.interfaceservice.dto.referencedata.OrderableDto;
import org.openlmis.interfaceservice.dto.referencedata.UserDto;
import org.openlmis.interfaceservice.repository.DhisDatasetRepository;
import org.openlmis.interfaceservice.service.referencedata.BaseReferenceDataService;
import org.openlmis.interfaceservice.service.referencedata.OrderableReferenceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MeasureReportService extends BaseReferenceDataService<UserDto> {

  @Value("service.url")
  private static String serviceUrl;

  private static final String MEASURE_REPORT_URL = serviceUrl + "/hapifhir/MeasureReport";
  private static final String LOCATION_URL = serviceUrl + "/hapifhir/Location";

  @Override
  protected String getUrl() {
    return MEASURE_REPORT_URL;
  }

  @Override
  protected Class<UserDto> getResultClass() {
    return null;
  }

  @Override
  protected Class<UserDto[]> getArrayResultClass() {
    return null;
  }

  @Autowired
  private OrderableReferenceDataService orderableReferenceDataService;

  @Autowired
  private DhisDatasetRepository repository;

  private Location getLocation(String responseEntity) {
    FhirContext ctx = FhirContext.forR4();

    IParser parser = ctx.newJsonParser();

    return parser.parseResource(Location.class, responseEntity);
  }

  private MeasureReport getMeasureReport(String responseEntity) {
    FhirContext ctx = FhirContext.forR4();

    IParser parser = ctx.newJsonParser();

    return parser.parseResource(MeasureReport.class, responseEntity);
  }

  /**
   * Get DHIS2 Dataset.
   *
   * @return List of DhisDataset
   */
  public List<DhisDataset> getDhisDataset(String startDate, String endDate,
                                          String measureReportUuid, String searchKey) {

    List<DhisDataset> dhisDatasetList = new ArrayList<>();
    List<Location> locations = getAllLocations();
    List<OrderableDto> products = orderableReferenceDataService.findAll();

    for (Location location : locations) {

      MeasureReport measureReport =
              getMeasureReport(getResult(MEASURE_REPORT_URL + "?measure=" + measureReportUuid
                      + "&periodStart=" + startDate + "&periodEnd=" + endDate + "&reporter"
                      + "=" + location.getId()).getBody());

      for (MeasureReportGroupComponent group : measureReport.getGroup()) {
        if (!group.getCode().getText().equals("programName")) {

          DhisDataset dhisDataset = new DhisDataset();

          dhisDataset.setDataElement(getProductMapId(products,
                  group.getCode().getText(), searchKey));

          dhisDataset.setOrgUnit(location.getIdentifier().get(1).getValue());
          dhisDataset.setValue(group.getMeasureScore().getValue().longValue());

          Calendar calendar = Calendar.getInstance();
          calendar.setTime(measureReport.getPeriod().getEnd());
          dhisDataset.setPeriod(calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH));

          dhisDatasetList.add(dhisDataset);
        }
      }
    }

    return dhisDatasetList;
  }

  private List<Location> getAllLocations() {

    List<Location> allLocations = new ArrayList<>();
    String locationEntity = getResult(LOCATION_URL).getBody();

    FhirContext ctx = FhirContext.forR4();
    IParser parser = ctx.newJsonParser();
    Bundle locationBundle = parser.parseResource(Bundle.class, locationEntity);
    List<BundleEntryComponent> entries = locationBundle.getEntry();

    for (BundleEntryComponent entry : entries) {
      Location location = (Location) entry.getResource();
      allLocations.add(location);
    }

    return allLocations;
  }


  /**
   * Get Products Map ID.
   *
   * @return Product Map ID
   */

  public String getProductMapId(List<OrderableDto> products, String productCode,
                                String searchKey) {

    for (OrderableDto orderableDto : products) {
      if (orderableDto.getProductCode() == productCode) {
        return orderableDto.getExtraData().get(searchKey).toString();
      }
    }
    return null;
  }

}
