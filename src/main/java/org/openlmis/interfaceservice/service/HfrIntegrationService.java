/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.interfaceservice.service;

import java.util.List;
import org.openlmis.interfaceservice.domain.HfrFacility;
import org.openlmis.interfaceservice.repository.HfrFacilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@SuppressWarnings("PMD.TooManyMethods")
public class HfrIntegrationService {

  @Autowired
  private HfrFacilityRepository repository;

  /**
   * Save HFR facility.
   */
  public HfrFacility saveHfrFacility(HfrFacility hfrFacility) {
    return repository.save(hfrFacility);
  }

  /**
   * Save HFR facility by HFR code.
   */
  public List<HfrFacility> getHfrFacilityByCode(String hfrCode) {
    return repository.getFacilityByHfrCode(hfrCode);
  }

  /**
   * Save get all facilities.
   */
  public List<HfrFacility> getAllFacilities() {
    return repository.findAll();
  }

}
