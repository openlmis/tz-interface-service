/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.interfaceservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.openlmis.interfaceservice.domain.HfrFacility;
import org.openlmis.interfaceservice.web.BaseDto;

@Setter
@Getter
public class HfrFacilityDto extends BaseDto implements HfrFacility.Exporter, HfrFacility.Importer {


  @JsonProperty("Comm_FacName")
  @NotNull
  private String commFacName;

  @JsonProperty("Council")
  @NotNull
  private String council;


  @JsonProperty("CreatedAt")
  @NotNull
  private Date createAt;


  @JsonProperty("District")
  @NotNull
  private String district;

  @JsonProperty("Fac_IDNumber")
  @NotNull
  private String facIdNumber;

  @JsonProperty("FacilityTypeGroup")
  @NotNull
  private String facilityTypeGroup;

  @JsonProperty("Latitude")
  @NotNull
  private String latitude;

  @JsonProperty("Longitude")
  @NotNull
  private String longitude;

  @JsonProperty("Name")
  @NotNull
  private String name;

  @JsonProperty("OSchangeClosedtoOperational")
  @NotNull
  @NotBlank
  private String osChangeClosedToOperational;

  @JsonProperty("OSchangeOpenedtoClose")
  @NotNull
  private String osChangeOpenedToClose;

  @JsonProperty("OperatingStatus")
  @NotNull
  private String operatingStatus;

  @JsonProperty("Ownership")
  @NotNull
  private String ownership;

  @JsonProperty("OwnershipGroup")
  @NotNull
  private String ownershipGroup;

  @JsonProperty("PostorUpdate")
  @NotNull
  private String postOrUpdate;

  @JsonProperty("Region")
  @NotNull
  private String region;

  @JsonProperty("RegistrationStatus")
  @NotNull
  private String registrationStatus;

  @JsonProperty("UpdatedAt")
  @NotNull
  private Date updatedAt;

  @JsonProperty("Village")
  @NotNull
  private String village;

  @JsonProperty("Ward")
  @NotNull
  private String ward;

  @JsonProperty("Zone")
  @NotNull
  private String zone;

  @JsonProperty("District_Code")
  @NotNull
  private String districtCode;

  @JsonProperty("Council_Code")
  @NotNull
  private String councilCode;

  @JsonProperty("FacilityTypeGroupCode")
  @NotNull
  private String facilityTypeGroupCode;

  @JsonProperty("OwnershipCode")
  @NotNull
  private String ownershipCode;

}
