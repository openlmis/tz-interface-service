/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.interfaceservice.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.when;
import static org.mockito.Mockito.verify;

import com.google.common.net.HttpHeaders;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.openlmis.interfaceservice.domain.HfrFacility;
import org.openlmis.interfaceservice.dto.HfrFacilityDto;
import org.openlmis.interfaceservice.dto.PageDto;
import org.openlmis.interfaceservice.exception.PermissionMessageException;
import org.openlmis.interfaceservice.exception.ValidationMessageException;
import org.openlmis.interfaceservice.i18n.MessageKeys;
import org.openlmis.interfaceservice.service.PermissionService;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;

@SuppressWarnings({"PMD.TooManyMethods"})
public class HfrIntegrationControllerIntegrationTest extends BaseWebIntegrationTest {

  private static final String RESOURCE_URL = "/api/hfrFacilities";

  private HfrFacility hfrFacility;
  private HfrFacilityDto hfrFacilityDto;
  private List<HfrFacility> hfrFacilityList = new ArrayList<>();
  PermissionMessageException exception;

  /**
   * Constructor.
   */
  public HfrIntegrationControllerIntegrationTest() {
    hfrFacility = new HfrFacility();
    hfrFacility.setName("HFR Facility");
    hfrFacility.setCommFacName("Tes");
    hfrFacility.setCouncil("Council");
    hfrFacility.setCouncilCode("Council Code");
    hfrFacility.setCreateAt(new Date());
    hfrFacility.setDistrict("District");
    hfrFacility.setFacIdNumber("ID Number");
    hfrFacility.setFacilityTypeGroup("Type Group");
    hfrFacility.setLatitude("Latitude");
    hfrFacility.setLongitude("Longitude");
    hfrFacility.setName("Name");
    hfrFacility.setOsChangeClosedToOperational("Test");
    hfrFacility.setOsChangeOpenedToClose("test");
    hfrFacility.setOperatingStatus("Operating");
    hfrFacility.setOwnership("Ownership");
    hfrFacility.setOwnershipGroup("Group");
    hfrFacility.setPostOrUpdate("Post");
    hfrFacility.setRegion("Region");
    hfrFacility.setFacilityTypeGroup("Group");
    hfrFacility.setOwnershipCode("code");
    hfrFacility.setRegistrationStatus("Status");
    hfrFacility.setUpdatedAt(new Date());
    hfrFacility.setVillage("Village");
    hfrFacility.setWard("Ward");
    hfrFacility.setZone("Zone");
    hfrFacility.setDistrictCode("Code");
    hfrFacility.setFacilityTypeGroupCode("Code");

    hfrFacilityDto = new HfrFacilityDto();
    hfrFacility.export(hfrFacilityDto);

    hfrFacilityList.add(hfrFacility);

    pageable = PageRequest.of(0, 10);

    exception = mockPermissionException(
            PermissionService.CREATE_FACILITY);

  }

  @Test
  public void shouldPostNewHfrFacility() {

    given(hfrIntegrationService.saveHfrFacility(hfrFacility)).willReturn(hfrFacility);

    HfrFacilityDto savedHfrFacility = restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(hfrFacilityDto)
            .when()
            .post(RESOURCE_URL)
            .then()
            .statusCode(201)
            .extract().as(HfrFacilityDto.class);

    verify(hfrIntegrationService).saveHfrFacility(hfrFacility);
    assertEquals("Name", savedHfrFacility.getName());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestWhenPostEmptyFields() {

    when(hfrIntegrationService.saveHfrFacility(hfrFacility))
            .thenThrow(new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_FIELD));

    restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(hfrFacilityDto)
            .when()
            .post(RESOURCE_URL)
            .then()
            .statusCode(400)
            .extract().as(HfrFacilityDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedForSavingHfrFacilitiesIfUserIsNotAuthorized() {

    when(hfrIntegrationService.saveHfrFacility(hfrFacility))
            .thenThrow(exception);

    restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(hfrFacilityDto)
            .when()
            .post(RESOURCE_URL)
            .then()
            .statusCode(403)
            .extract().as(HfrFacilityDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldUpdateHfrFacility() {

    given(hfrIntegrationService.saveHfrFacility(hfrFacility)).willReturn(hfrFacility);

    HfrFacilityDto savedHfrFacility = restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(hfrFacilityDto)
            .put(RESOURCE_URL)
            .then()
            .statusCode(202)
            .extract().as(HfrFacilityDto.class);

    assertEquals("Name", savedHfrFacility.getName());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestWhenPostEmptyFieldsDuringUpdate() {

    when(hfrIntegrationService.saveHfrFacility(hfrFacility))
            .thenThrow(new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_FIELD));

    restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(hfrFacilityDto)
            .when()
            .put(RESOURCE_URL)
            .then()
            .statusCode(400)
            .extract().as(HfrFacilityDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedForUpdatingHfrFacilitiesIfUserIsNotAuthorized() {

    when(hfrIntegrationService.saveHfrFacility(hfrFacility))
            .thenThrow(exception);

    restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(hfrFacilityDto)
            .when()
            .put(RESOURCE_URL)
            .then()
            .statusCode(403)
            .extract().as(HfrFacilityDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnPageOfHfrFacilities() {
    when(hfrIntegrationService.getAllFacilities()).thenReturn(hfrFacilityList);

    PageDto resultPage = restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .when()
            .get(RESOURCE_URL)
            .then()
            .statusCode(200)
            .extract().as(PageDto.class);

    assertEquals(1, resultPage.getContent().size());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }
}
