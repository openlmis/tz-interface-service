/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.interfaceservice.web;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.openlmis.interfaceservice.domain.HfrFacility;
import org.openlmis.interfaceservice.dto.HfrFacilityDto;
import org.openlmis.interfaceservice.service.HfrIntegrationService;
import org.openlmis.interfaceservice.service.PermissionService;
import org.openlmis.interfaceservice.validate.HfrFacilityValidator;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;

@SuppressWarnings({"PMD.UnusedPrivateField"})
public class HfrIntegrationControllerTest {

  @InjectMocks
  HfrIntegrationController hfrIntegrationController = new HfrIntegrationController();

  @Mock
  HfrIntegrationService service;

  @Mock
  PermissionService permissionService;

  @Mock
  HfrFacilityValidator validator;

  @Mock
  Pageable pageable;

  HfrFacility hfrFacility;
  HfrFacilityDto hfrFacilityDto;

  List<HfrFacility> hfrFacilityList = new ArrayList<>();

  BindingResult result = mock(BindingResult.class);

  /**
   * Constructor.
   */
  public HfrIntegrationControllerTest() {
    initMocks(this);
    hfrFacility = new HfrFacility();
    hfrFacility.setName("HFR Facility");
    hfrFacility.setCommFacName("Tes");
    hfrFacility.setCouncil("Council");
    hfrFacility.setCouncilCode("Council Code");
    hfrFacility.setCreateAt(new Date());
    hfrFacility.setDistrict("District");
    hfrFacility.setFacIdNumber("ID Number");
    hfrFacility.setFacilityTypeGroup("Type Group");
    hfrFacility.setLatitude("Latitude");
    hfrFacility.setLongitude("Longitude");
    hfrFacility.setName("Name");
    hfrFacility.setOsChangeClosedToOperational("Test");
    hfrFacility.setOsChangeOpenedToClose("test");
    hfrFacility.setOperatingStatus("Operating");
    hfrFacility.setOwnership("Ownership");
    hfrFacility.setOwnershipGroup("Group");
    hfrFacility.setPostOrUpdate("Post");
    hfrFacility.setRegion("Region");
    hfrFacility.setFacilityTypeGroup("Group");
    hfrFacility.setOwnershipCode("code");
    hfrFacility.setRegistrationStatus("Status");
    hfrFacility.setUpdatedAt(new Date());
    hfrFacility.setVillage("Village");
    hfrFacility.setWard("Ward");
    hfrFacility.setZone("Zone");
    hfrFacility.setDistrictCode("Code");
    hfrFacility.setFacilityTypeGroupCode("Code");

    hfrFacilityDto = new HfrFacilityDto();
    hfrFacility.export(hfrFacilityDto);
    hfrFacilityList.add(hfrFacility);

    when(service.saveHfrFacility(hfrFacility))
            .thenReturn(hfrFacility);

    when(service.getAllFacilities()).thenReturn(hfrFacilityList);
  }

  @Test
  public void shouldSaveNewFacility() {
    assertEquals(hfrFacility.getName(),
            hfrIntegrationController.saveNewFacility(hfrFacilityDto, result).getName());
  }

  @Test
  public void shouldGetFacilities() {
    assertEquals(hfrFacilityList.size(),
            hfrIntegrationController.getAllHfrFacility(pageable).getTotalElements());

  }
}